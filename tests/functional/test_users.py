"""
This file (test_users.py) contains the functional tests for the `users` blueprint.
"""


def test_home_page(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/' page is requested (GET)
    THEN check that the response is valid
    """
    response = test_client.get('/')
    assert response.status_code == 200
    assert b'Flask Book Reviews' in response.data


def test_get_registration_page(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/register' page is requested (GET)
    THEN check the response is valid
    """
    response = test_client.get('/register')
    assert response.status_code == 200
    assert b'Flask Book Reviews' in response.data
    assert b'User Registration' in response.data


def test_valid_registration(test_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/register' page is posted to (POST)
    THEN check the response is valid
    """
    response = test_client.post('/register',
                                data=dict(email='patkennedy79@gmail.com',
                                          password='FlaskIsAwesome',
                                          confirm='FlaskIsAwesome'),
                                follow_redirects=True)
    assert response.status_code == 200
    assert b'Thank you for registering, patkennedy79@gmail.com!' in response.data
    assert b'Flask Book Reviews' in response.data


def test_invalid_registration(test_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/register' page is posted to with invalid credentials (POST) - passwords do not match
    THEN check an error message is returned to the user
    """
    response = test_client.post('/register',
                                data=dict(email='patkennedy79@hotmail.com',
                                          password='FlaskIsAwesome',
                                          confirm='FlskIsAwesome'),   # Does NOT match!
                                follow_redirects=True)
    assert response.status_code == 200
    assert b'Thank you for registering, patkennedy79@hotmail.com!' not in response.data
    assert b'[This field is required.]' not in response.data
    assert b'Error(s) in the form data... please check that all fields are filled out and passwords match.' in response.data
    assert b'Flask Book Reviews' in response.data


def test_invalid_registration_2(test_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/register' page is posted to with invalid credentials (POST) - email field is missing
    THEN check an error message is returned to the user
    """
    response = test_client.post('/register',
                                data=dict(password='FlaskIsAwesome',
                                          confirm='FlaskIsAwesome'),   # Email field is missing!
                                follow_redirects=True)
    assert response.status_code == 200
    assert b'Thank you for registering, patkennedy79@hotmail.com!' not in response.data
    assert b'[This field is required.]' in response.data
    assert b'Error(s) in the form data... please check that all fields are filled out and passwords match.' in response.data
    assert b'Flask Book Reviews' in response.data


def test_duplicate_registration(test_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/register' page is posted to (POST) using an email address already registered
    THEN check an error message is returned to the user
    """
    # Register the new account
    test_client.post('/register',
                     data=dict(email='pkennedy@hey.com',
                               password='FlaskIsTheBest',
                               confirm='FlaskIsTheBest'),
                     follow_redirects=True)

    # Try registering with the same email address
    response = test_client.post('/register',
                                data=dict(email='pkennedy@hey.com',
                                          password='FlaskIsStillTheBest',
                                          confirm='FlaskIsStillTheBest'),
                                follow_redirects=True)
    assert response.status_code == 200
    assert b'ERROR! Email (pkennedy@hey.com) already exists in the database.' in response.data
    assert b'Thank you for registering, pkennedy@hey.com!' not in response.data


def test_login_page(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/login' page is requested (GET)
    THEN check the response is valid
    """
    response = test_client.get('/login')
    assert response.status_code == 200
    assert b'Login' in response.data
    assert b'Email' in response.data
    assert b'Password' in response.data


def test_valid_login_logout(test_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/login' page is posted to (POST)
    THEN check the response is valid
    """
    response = test_client.post('/login',
                                data=dict(email='patkennedy79@gmail.com', password='FlaskIsAwesome'),
                                follow_redirects=True)
    assert response.status_code == 200
    assert b'Thank you for logging in, patkennedy79@gmail.com!' in response.data
    assert b'Flask Book Reviews' in response.data
    assert b'Logout' in response.data
    assert b'Login' not in response.data
    assert b'Register' not in response.data

    """
    GIVEN a Flask application configured for testing
    WHEN the '/logout' page is requested (GET)
    THEN check the response is valid
    """
    response = test_client.get('/logout', follow_redirects=True)
    assert response.status_code == 200
    assert b'Goodbye!' in response.data
    assert b'Flask Book Reviews' in response.data
    assert b'Logout' not in response.data
    assert b'Login' in response.data
    assert b'Register' in response.data


def test_invalid_login(test_client, init_database):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/login' page is posted to with invalid credentials (POST)
    THEN check an error message is returned to the user
    """
    response = test_client.post('/login',
                                data=dict(email='patkennedy79@gmail.com', password='FlaskIsNotAwesome'),
                                follow_redirects=True)
    assert response.status_code == 200
    assert b'ERROR! Incorrect login credentials.' in response.data
    assert b'Flask Book Reviews' in response.data
    assert b'Logout' not in response.data
    assert b'Login' in response.data
    assert b'Register' in response.data


def test_login_already_logged_in(test_client, init_database, log_in_default_user):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/login' page is posted to (POST) when the user is already logged in
    THEN check an error message is returned to the user
    """
    response = test_client.post('/login',
                                data=dict(email='patkennedy79@gmail.com', password='FlaskIsAwesome'),
                                follow_redirects=True)
    assert response.status_code == 200
    assert b'Already logged in!  Redirecting to homepage...' in response.data
    assert b'Flask Book Reviews' in response.data
    assert b'Logout' in response.data
    assert b'Login' not in response.data
    assert b'Register' not in response.data


def test_registration_when_logged_in(test_client, log_in_default_user):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/register' page is posted to (POST) when the user is logged in
    THEN check an error message is returned to the user
    """
    response = test_client.post('/register',
                                data=dict(email='patkennedy79@gmail.com',
                                          password='FlaskIsAwesome',
                                          confirm='FlaskIsAwesome'),
                                follow_redirects=True)
    assert response.status_code == 200
    assert b'Already logged in!  Redirecting to homepage...' in response.data
    assert b'Thank you for registering, pkennedy@hey.com!' not in response.data


def test_status_page(test_client):
    """
    GIVEN a Flask application configured for testing
    WHEN the '/status' page is requested (GET)
    THEN check the response is valid
    """
    response = test_client.get('/status')
    assert response.status_code == 200
