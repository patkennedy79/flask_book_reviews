from project import database, create_app
import os
import pytest


# --------
# Fixtures
# --------

@pytest.fixture(scope='module')
def test_client():
    # Set the Testing configuration prior to creating the Flask application
    os.environ['CONFIG_TYPE'] = 'config.TestingConfig'
    flask_app = create_app()
    print(f"1test_client().... os.environ['CONFIG_TYPE']: {os.environ['CONFIG_TYPE']}.. database: {database}")

    # Create a test client using the Flask application configured for testing
    with flask_app.test_client() as testing_client:
        # Establish an application context
        with flask_app.app_context():
            print(f"2test_client()... before yield... database: {database}")
            yield testing_client  # this is where the testing happens!
            print("3test_client()... after yield")


@pytest.fixture(scope='module')
def init_database(test_client):
    print(f"init_database().... os.environ['CONFIG_TYPE']: {os.environ['CONFIG_TYPE']}")
    print(dir(database))
    # Create the database and the database table
    database.create_all_tables()

    yield  # this is where the testing happens!

    # Clear the database after testing
    database.drop_all_tables()


@pytest.fixture(scope='function')
def log_in_default_user(test_client):
    test_client.post('/login',
                     data={'email': 'patkennedy79@gmail.com', 'password': 'FlaskIsAwesome'})

    yield  # this is where the testing happens!

    test_client.get('/logout')
