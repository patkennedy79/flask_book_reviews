import os


# Determine the folder of the top-level directory of this project
BASEDIR = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    FLASK_ENV = 'development'
    DEBUG = False
    TESTING = False
    SECRET_KEY = os.getenv('SECRET_KEY', default='BAD_SECRET_KEY')
    DATABASE_HOST = os.getenv('DATABASE_HOST', default='127.0.0.1')
    DATABASE_NAME = os.getenv('DATABASE_NAME', default='flask-books-db')
    DATABASE_USER = os.getenv('DATABASE_USER', default='postgres')
    DATABASE_PASSWORD = os.getenv('DATABASE_PASSWORD', default='tomatoes')


class ProductionConfig(Config):
    FLASK_ENV = 'production'


class DevelopmentConfig(Config):
    DEBUG = True


class TestingConfig(Config):
    TESTING = True
    DATABASE_FILE = os.getenv('TEST_DATABASE_FILE', default=f"{os.path.join(BASEDIR, 'instance', 'test.db')}")
    WTF_CSRF_ENABLED = False
