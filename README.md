## Overview

This Flask application manages the book reviews for multiple users.

## Installation Instructions

### Installation

Pull down the source code from this GitLab repository:

```sh
$ git clone git@gitlab.com:patkennedy79/flask_book_reviews.git
```

Create a new virtual environment:

```sh
$ cd flask_book_reviews
$ python3 -m venv venv
```

Activate the virtual environment:

```sh
$ source venv/bin/activate
```

Install the python packages specified in requirements.txt:

```sh
(venv) $ pip install -r requirements.txt
```

### Running the Flask Application

Run development server to serve the Flask application:

```sh
(venv) $ flask --app app --debug run
```

Navigate to 'http://127.0.0.1:5000' in your favorite web browser to view the website!

## Key Python Modules Used

* **Flask**: micro-framework for web application development which includes the following dependencies:
  * click: package for creating command-line interfaces (CLI)
  * itsdangerous: cryptographically sign data 
  * Jinja2: templating engine
  * MarkupSafe: escapes characters so text is safe to use in HTML and XML
  * Werkzeug: set of utilities for creating a Python application that can talk to a WSGI server
* **pytest**: framework for testing Python projects

This application is written using Python 3.11.

## Testing

To run all the tests:

```sh
(venv) $ python -m pytest -v
```

## Database

After installing Docker Desktop, check that Docker is installed and running:

```sh
$ docker --version
Docker version 20.10.22, build 3a2c30b
```

Pull down the latest Postgres image:

```sh
$ docker pull postgres
```

Start a new container running a Postgres server and creating a new database:

```sh
$ docker run --name flask-books-db -e POSTGRES_PASSWORD=tomatoes -e POSTGRES_DB=flask-books-db -p 5432:5432 -d postgres
```

Here are all the options used:

* `--name` - name of the Postgres container
* `-e` - environment variable applicable to the Postgres container:
  * 'POSTGRES_PASSWORD' - database password (*required*)
  * 'POSTGRES_DB' - database name that gets created when the container starts (*optional*, default database created is `postgres`)
* `-p` - port mapping of the host port on your computer that maps to the PostgreSQL container port inside the container
* `-d` - run in daemon mode (container keeps running in the background)
