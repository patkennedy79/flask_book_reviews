from flask import g
import sqlite3
import psycopg2

from .models import User, UserModel
from prettytable import from_db_cursor


class Database:
    """Interface for connecting to a relational database (PostgreSQL or SQLite)."""

    POSTRESQL_DATABASE = 1
    SQLITE_DATABASE = 2

    def __init__(self):
        self.database_host = None      # Only applicable to PostgreSQL
        self.database_name = None      # Only applicable to PostgreSQL
        self.database_user = None      # Only applicable to PostgreSQL
        self.database_password = None  # Only applicable to PostgreSQL
        self.database_file = None      # Only applicable to SQLite
        self.database_type = None
        self.tables = {
            'users': User()
        }

    def __repr__(self):
        if self.database_type == self.POSTRESQL_DATABASE:
            return f"PostgreSQL database... host: {self.database_host}, name: {self.database_name}"
        elif self.database_type == self.SQLITE_DATABASE:
            return f"SQLite database... file: {self.database_file}, name: {self.database_name}"
        return "Database has not been properly configured!"

    def initialize(self, **kwargs):
        """Initialize the configuration parameters for connecting to the database."""
        for key, value in kwargs.items():
            if key == 'host':
                self.database_host = kwargs['host']
            if key == 'name':
                self.database_name = kwargs['name']
            if key == 'user':
                self.database_user = kwargs['user']
            if key == 'password':
                self.database_password = kwargs['password']
            if key == 'file':
                self.database_file = kwargs['file']

        if self.database_host is not None and self.database_user is not None and self.database_password is not None:
            self.database_type = self.POSTRESQL_DATABASE
            print(f"Configured PostgreSQL database!")
        if self.database_file is not None:
            self.database_type = self.SQLITE_DATABASE
            print(f"Configured SQLite database!")

    def get_database_connection(self):
        """Returns the connection to the relational database."""
        print(f'Connecting to the database ({self.database_name})...')

        if 'db' not in g:
            if self.database_type == self.POSTRESQL_DATABASE:
                g.db = psycopg2.connect(
                    host=self.database_host,
                    database=self.database_name,
                    user=self.database_user,
                    password=self.database_password
                )
            elif self.database_type == self.SQLITE_DATABASE:
                g.db = sqlite3.connect(self.database_file)
            else:
                raise Exception('PostgreSQL or SQLite database has not been configured!')
        return g.db

    def close_database_connection(self, response):
        """Close the connection to the relational database."""
        db = g.pop('db', None)

        if db is not None:
            # Close the communication with the database
            print(f'\nClosing connection to the relational database ({self.database_name}).')
            db.close()

        return response

    def get_database_version(self):
        """Returns the database version information."""
        db_cursor = self.get_database_connection().cursor()
        if self.database_type == self.POSTRESQL_DATABASE:
            db_cursor.execute('SELECT version();')
        elif self.database_type == self.SQLITE_DATABASE:
            db_cursor.execute('SELECT sqlite_version();')
        else:
            raise Exception('PostgreSQL or SQLite database has not been configured to get version info!')
        database_version = db_cursor.fetchone()
        print(f'Database version: {database_version}')
        db_cursor.close()
        return database_version

    def create_all_tables(self):
        """Create all the tables of the database."""

        # The `db_connection` object encapsulates a database session.  It is used to:
        #   1. create a new cursor object
        #   2. terminate database transactions using either:
        #      - commit() - success condition where database updates are saved (persist)
        #      - rollback() - failure condition where database updates are discarded
        db_connection = self.get_database_connection()

        # The `db_cursor` object allows the following interactions with the database:
        #   1. send SQL commands to the database using `execute()` or `executemany()`
        #   2. retrieve data from the database using `fetchone()`, `fetchmany()`, or `fetchall()`
        db_cursor = db_connection.cursor()
        for table in self.tables.values():
            table.create_table(db_cursor)
        db_connection.commit()

    def drop_all_tables(self):
        """Drop all the tables of the database."""
        db_connection = self.get_database_connection()
        db_cursor = db_connection.cursor()
        for table in self.tables.values():
            table.drop_table(db_cursor)
        db_connection.commit()

    def print_users_table_schema(self):
        """Print the schema of the users table."""
        db_cursor = self.get_database_connection().cursor()
        for table in self.tables.values():
            table.print_table_schema(db_cursor)

    def init_app(self, app):
        """Initialize the database connection with the Flask application."""
        app.teardown_appcontext(self.close_database_connection)

    # -----------
    # Users Table
    # -----------

    def add_new_user(self, email: str, password_plaintext: str):
        """Add a new user to the users table."""
        db_connection = self.get_database_connection()
        db_cursor = db_connection.cursor()

        try:
            db_cursor.execute(self.tables["users"].add(email, password_plaintext))
            db_connection.commit()
        except psycopg2.errors.UniqueViolation as e1:
            print(e1)
            db_connection.rollback()
            raise ValueError(f"ERROR! Email ({email}) already exists in the database.")
        except sqlite3.IntegrityError as e2:
            print(e2)
            db_connection.rollback()
            raise ValueError(f"ERROR! Email ({email}) already exists in the database.")

    def print_all_users(self):
        """Print all users to the console."""
        db_cursor = self.get_database_connection().cursor()
        db_cursor.execute(self.tables["users"].read_all())
        table = from_db_cursor(db_cursor)
        print(table)

    def delete_user(self, id: int):
        """Delete a row from the users table."""
        db_connection = self.get_database_connection()
        db_cursor = db_connection.cursor()
        db_cursor.execute(self.tables["users"].delete(id))
        if db_cursor.rowcount > 0:
            print(f"Deleted {db_cursor.rowcount} row(s) from the users table!")
            db_connection.commit()

    def print_user(self, id: int):
        """Prints a row from the users table."""
        db_cursor = self.get_database_connection().cursor()
        db_cursor.execute(self.tables["users"].read(id))
        table = from_db_cursor(db_cursor)
        print(table)

    def update_user_password(self, id: int, password_plaintext: str):
        """Update the password for a row in the users table."""
        db_connection = self.get_database_connection()
        db_cursor = db_connection.cursor()
        db_cursor.execute(self.tables["users"].update_password(id, password_plaintext))
        if db_cursor.rowcount > 0:
            print(f"Updated password for id {id} in the users table ({db_cursor.rowcount} row(s) updated)!")
            db_connection.commit()

    def update_user_email(self, id: int, email: str):
        """Update the email for a row in the users table."""
        db_connection = self.get_database_connection()
        db_cursor = db_connection.cursor()
        db_cursor.execute(self.tables["users"].update_email(id, email))
        if db_cursor.rowcount > 0:
            print(f"Updated email address for id {id} in the users table ({db_cursor.rowcount} row(s) updated)!")
            db_connection.commit()

    def get_user_by_id(self, id: str) -> UserModel:
        """Returns the User object (or None) based on the specified ID."""
        db_connection = self.get_database_connection()
        db_cursor = db_connection.cursor()
        db_cursor.execute(self.tables["users"].read(int(id)))
        if db_cursor.rowcount == 0:
            return None
        row = db_cursor.fetchone()
        print(row)
        return UserModel(
            id=row[0],
            email=row[1],
            hashed_password=row[2],
            registered_on=row[3]
        )

    def get_user_by_email(self, email: str) -> UserModel:
        """Returns the User object (or None) based on the specified email."""
        db_connection = self.get_database_connection()
        db_cursor = db_connection.cursor()
        db_cursor.execute(self.tables["users"].read_using_email(email))
        if db_cursor.rowcount == 0:
            return None
        row = db_cursor.fetchone()
        print(row)
        return UserModel(
            id=row[0],
            email=row[1],
            hashed_password=row[2],
            registered_on=row[3]
        )
