from . import users_blueprint
from .forms import RegisterForm, LoginForm
from project import database
from flask import render_template, request, flash, redirect, url_for
import click
from flask_login import current_user, login_required, login_user, logout_user
import os


# ---
# CLI
# ---

@users_blueprint.cli.command('print_users_table_schema')
def cli_print_users_table_schema():
    """Print the schema of the users table in the database."""
    database.print_users_table_schema()


@users_blueprint.cli.command('add_new_user')
@click.argument('email')
@click.argument('password')
def cli_add_new_user(email: str, password: str):
    """Add a new row to the users table in the database."""
    database.add_new_user(email, password)


@users_blueprint.cli.command('print_all_users')
def cli_print_all_users():
    """Print all the rows in the users table in the database."""
    database.print_all_users()


@users_blueprint.cli.command('delete_user')
@click.argument('id')
def cli_delete_user(id: int):
    """Delete a row from the users table in the database."""
    database.delete_user(id)


@users_blueprint.cli.command('print_user')
@click.argument('id')
def cli_print_user(id: int):
    """Print a row from the users table in the database."""
    database.print_user(id)


@users_blueprint.cli.command('update_user_password')
@click.argument('id')
@click.argument('password')
def cli_update_user_password(id: int, password: str):
    """Update the password for a user in the users table in the database."""
    database.update_user_password(id, password)


@users_blueprint.cli.command('update_user_email')
@click.argument('id')
@click.argument('email')
def cli_update_user_email(id: int, email: str):
    """Update the email address for a user in the users table in the database."""
    database.update_user_email(id, email)


@users_blueprint.cli.command('get_user_by_id')
@click.argument('id')
def cli_get_user_by_id(id: int):
    print(database.get_user_by_id(id))


# ------
# Routes
# ------

@users_blueprint.get('/')
def index():
    return render_template('users/index.html')


@users_blueprint.get('/status')
def status():
    return render_template('users/status.html',
                           config_type=os.getenv('CONFIG_TYPE', default='config.DevelopmentConfig'),
                           database_version=database.get_database_version())


@users_blueprint.route('/register', methods=['GET', 'POST'])
def register():
    # If the User is already logged in, don't allow them to try to register
    if current_user.is_authenticated:
        flash('Already logged in!  Redirecting to homepage...')
        return redirect(url_for('users.index'))

    form = RegisterForm()

    if request.method == 'POST':
        if form.validate_on_submit():
            try:
                database.add_new_user(form.email.data, form.password.data)
                flash(f'Thank you for registering, {form.email.data}!')
                return redirect(url_for('users.index'))
            except ValueError as e:
                flash(e, 'error')
        else:
            print(f"Form validation error(s): {form.errors}")
            flash('Error(s) in the form data... please check that all fields are filled out and passwords match.')
    return render_template('users/register.html', form=form)


@users_blueprint.route('/login', methods=['GET', 'POST'])
def login():
    # If the User is already logged in, don't allow them to try to log in again
    if current_user.is_authenticated:
        flash('Already logged in!  Redirecting to homepage...')
        return redirect(url_for('users.index'))

    form = LoginForm()

    if request.method == 'POST':
        if form.validate_on_submit():
            user = database.get_user_by_email(form.email.data)
            if user and user.is_password_correct(form.password.data):
                login_user(user, remember=form.remember_me.data)
                print(user)
                flash('Thank you for logging in, {}!'.format(current_user.email))
                return redirect(url_for('users.index'))

        flash('ERROR! Incorrect login credentials.')
    return render_template('users/login.html', form=form)


@users_blueprint.route('/logout')
@login_required
def logout():
    logout_user()
    flash('Goodbye!')
    return redirect(url_for('users.index'))
