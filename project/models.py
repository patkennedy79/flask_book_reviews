from datetime import date
from abc import ABC, abstractmethod
from werkzeug.security import check_password_hash, generate_password_hash
from flask_login import UserMixin


# ------------------------------------------
# Database Table - Abstract Base Class (ABC)
# ------------------------------------------
class DatabaseTable(ABC):
    """Abstract class for defining a database table."""

    @abstractmethod
    def create_table(self, db_cursor):
        pass

    @abstractmethod
    def drop_table(self, db_cursor):
        pass

    @abstractmethod
    def print_table_schema(self, db_cursor):
        pass


# ---------------
# Database Tables
# ---------------
class User(DatabaseTable):
    """Table for storing data about the users.

    Columns:
    - id (int): primary key (auto-incrementing)
    - email (str): email address of the user
    - hashed_password (str): hashed password (using werkzeug.security)
    - registered_on (date): date when the user registered their account

    REMEMBER: Never store the plaintext password in a database!
    """

    def create_table(self, db_cursor):
        """Create the schema for the users table."""

        # Create the schema (i.e. columns) for the users table
        # NOTE: The keyword SERIAL is used in postgreSQL for an
        #       auto-incrementing primary key.  In other relational
        #       database systems, this would be "PRIMARY KEY AUTO_INCREMENT".
        db_cursor.execute("""
            CREATE TABLE users(
                id SERIAL PRIMARY KEY,
                email VARCHAR(200) NOT NULL UNIQUE,
                hashed_password VARCHAR(128) NOT NULL,
                registered_on DATE NOT NULL
            );
        """)

    def drop_table(self, db_cursor):
        """Drop the table from the database."""
        db_cursor.execute('DROP TABLE IF EXISTS users;')

    def print_table_schema(self, db_cursor):
        """Print the schema for the table."""
        db_cursor.execute("""
            SELECT table_name, column_name, data_type
            FROM information_schema.columns
            WHERE table_name = 'users';
        """)
        print('\nusers table schema:')
        for row in db_cursor.fetchall():
            print(row)

    def add(self, email: str, password_plaintext: str) -> str:
        """Returns the SQL statement to a new user to the users table."""
        return f"""
            INSERT INTO users(email, hashed_password, registered_on)
            VALUES ('{email}', '{self._generate_password_hash(password_plaintext)}', '{date.today()}');
        """

    def read_all(self) -> str:
        """Returns the SQL statement to retrieve all rows of the users table."""
        return 'SELECT * FROM users;'

    def read(self, row_id: int) -> str:
        """Returns the SQL statement to retrieve a specific row from the users table."""
        return f"""
            SELECT * FROM users
            WHERE id = '{row_id}';
        """

    def read_using_email(self, email: str) -> str:
        """Returns the SQL statement to retrieve a specific row from the users table."""
        return f"""
            SELECT * FROM users
            WHERE email = '{email}';
        """

    def update_password(self, id: int, password_plaintext: str) -> str:
        """Returns the SQL statement to update the password for a specific row in the users table."""
        return f"""
            UPDATE users
            SET hashed_password = '{self._generate_password_hash(password_plaintext)}'
            WHERE id = {id};
        """

    def update_email(self, id: int, email: str) -> str:
        """Returns the SQL statement to update the email for a specific row in the users table."""
        return f"""
            UPDATE users
            SET email = '{email}'
            WHERE id = {id};
        """

    def delete(self, id: int) -> str:
        """Returns the SQL statement to delete a specific row from the users table."""
        return f"DELETE FROM users WHERE id = {id};"

    @staticmethod
    def _generate_password_hash(password_plaintext):
        return generate_password_hash(password_plaintext)


class UserModel(UserMixin):
    """Model for a user in the users table.

    Attributes:
    - id (int): primary key (auto-incrementing)
    - email (str): email address of the user
    - hashed_password (str): hashed password (using werkzeug.security)
    - registered_on (date): date when the user registered their account

    REMEMBER: Never store the plaintext password in a database!
    """

    def __init__(self, id: int, email: str, hashed_password: str, registered_on: date):
        self.id = id
        self.email = email
        self.hashed_password = hashed_password
        self.registered_on = registered_on

    def __repr__(self):
        return f"UserModel<{self.id}, {self.email}>"

    def is_password_correct(self, plaintext_password: str):
        return check_password_hash(self.hashed_password, plaintext_password)
