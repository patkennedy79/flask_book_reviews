from flask import Flask
import click
import os
from flask_login import LoginManager

from .database import Database


# -------------
# Configuration
# -------------

# Create the instances of the Flask extensions in the global scope, but without any arguments passed in.
# These instances are not attached to the application at this point.
database = Database()
login_manager = LoginManager()
login_manager.login_view = "users.login"


# ----------------------------
# Application Factory Function
# ----------------------------

def create_app():
    # Create the Flask application
    app = Flask(__name__)
    # Configure the Flask application
    config_type = os.getenv('CONFIG_TYPE', default='config.DevelopmentConfig')
    app.config.from_object(config_type)

    # Initialize the database
    global database
    if config_type == 'config.TestingConfig':
        database.initialize(file=app.config['DATABASE_FILE'])
    else:
        database.initialize(
            host=app.config['DATABASE_HOST'],
            name=app.config['DATABASE_NAME'],
            user=app.config['DATABASE_USER'],
            password=app.config['DATABASE_PASSWORD']
        )

    initialize_extensions(app)
    register_blueprints(app)
    register_cli_commands(app)
    return app


def initialize_extensions(app):
    database.init_app(app)
    login_manager.init_app(app)

    @login_manager.user_loader
    def load_user(user_id):
        return database.get_user_by_id(user_id)


def register_blueprints(app):
    # Since the application instance is now created, register each Blueprint
    # with the Flask application instance (app)
    from project.users import users_blueprint

    app.register_blueprint(users_blueprint)


def register_cli_commands(app):
    @app.cli.command('print_db_version')
    def cli_print_db_version():
        """Print the database version."""
        click.echo(f"Database version: {database.get_database_version()}")

    @app.cli.command('init_db')
    def cli_init_db():
        """Initialize the database by creating all the tables."""
        database.create_all_tables()
        click.echo("Created all tables in the database!")

    @app.cli.command('clear_db')
    def cli_clear_db():
        """Drop all tables in the database."""
        database.drop_all_tables()
        click.echo("Cleared the database by dropping all tables!")
